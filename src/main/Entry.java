package main;

/**
 * 
 * @author Murarasu
 *
 */

public class Entry {

	public static void main(String[] args) {
		m1();
	}

	public static void m1() {

		int startInterval = 1000;
		int stopInterval = 10000;
		int contor = 0;
		int divizor = 67;
		int restCautat = 23;
		int numarValid = 0;

		int restCurent = (startInterval % divizor);
		if (restCurent < restCautat) {
			numarValid = startInterval + (restCautat - restCurent);
		} else if (restCurent > restCautat) {
			numarValid = startInterval + (divizor - (restCurent - restCautat));
		} else {
			numarValid = startInterval;
		}

		for (int i = numarValid; i < stopInterval; i += divizor) {
			contor++;
		}

		System.out.println(contor);
	}
}
